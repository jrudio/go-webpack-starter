package webpack

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

type Config struct {
	Path          string
	DisplayOutput bool
	Args          []string
}

var log func(txt string)

// Only tested on OS X Yosemite
func Start(config *Config) {
	targetPath := config.Path
	displayOutput := config.DisplayOutput

	log = initLog(displayOutput)

	// Check that webpack exists in PATH
	wpPath, err := exec.LookPath("webpack")
	if err != nil {
		log(err.Error())
		return
	}

	log("Found webpack")

	wpCmd := command(wpPath, config.Args)

	// if target starts with '/' then it's an absolute path
	if string(targetPath[0]) == "/" {
		wpCmd.Dir = targetPath
	} else {
		// Get the current path
		cmd := exec.Command("pwd")

		var cmdOut bytes.Buffer

		cmd.Stdout = &cmdOut

		// Execute command
		err := cmd.Run()

		if err != nil {
			log(err.Error())
			return
		}

		// Convert to string
		currDir := cmdOut.String()

		// Trim any whitespace
		currDir = strings.TrimSpace(currDir)

		// Remove './' if present else just append to currDir
		if string(targetPath[0:2]) == "./" {
			targetPath = targetPath[2:]
		}

		// Append the target dir
		wpCmd.Dir = currDir + "/" + targetPath
	}

	if displayOutput {
		wpCmd.Stdout = os.Stdout
		wpCmd.Stderr = os.Stderr
	}

	log("Starting webpack")

	err = wpCmd.Start()

	if err != nil {
		log(err.Error())
		return
	}
}

// command reimplements exec.Command to use a slice of strings instead strings
func command(name string, args []string) *exec.Cmd {
	cmd := &exec.Cmd{
		Path: name,
		Args: append([]string{name}, args...),
	}
	if filepath.Base(name) == name {
		if lp, err := exec.LookPath(name); err != nil {
			// cmd.lookPathErr = err
			log(err.Error())
		} else {
			cmd.Path = lp
		}
	}
	return cmd
}

func initLog(displayOutput bool) func(txt string) {
	return func(txt string) {
		if displayOutput {
			fmt.Println(txt)
		}
	}
}
