A little helper package to start WebPack when you start up your Go webserver

Requirements
=====
Have webpack installed and available in your $PATH

This package looks in your $PATH to see if `webpack` is available so make sure you `npm i -g webpack`.
Usage
====
`go get github.com/jrudio/go-webpack-starter`

####Start webpack

```go
  import "github.com/jrudio/go-webpack-starter"
  config := webpack.Config{
    Path: "./src",
    DisplayOutput: true,
    Args: []string{"--progress", "--colors", "--watch"}
  }
  webpack.Start(config)
```

Note
====
Not meant to be a totally serious package. I know one can just create a shell script to start webpack.
Tested only on OS X 10.10.5
I don't expect Windows to work with this.


#####The Ideal Scenario

```go
package main

import (
    "log"
    "net/http"
    webpack "github.com/jrudio/go-webpack-starter"
)

func main() {
    //  Path can be absolute or relative
    config := webpack.Config{
      Path: "./src",
      DisplayOutput: true,
      Args: []string{"--progress", "--colors", "--watch"}
    }

    go webpack.Start(config)

    log.Fatal(http.ListenAndServe(":8080", nil))
}

```
